
# (c) copyright 2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import cairo
from gi.repository import Gdk
from gi.repository import GLib
from gi.repository import Gio
from libkusozako3.Entity import DeltaEntity
from . import Painters


class DeltaEntryPoint(DeltaEntity):

    def _get_base_surface(self):
        _, source_path = GLib.file_open_tmp(None)
        _, target_path = GLib.file_open_tmp(None)
        root_window = Gdk.get_default_root_window()
        window_geometry = root_window.get_geometry()
        pixbuf = Gdk.pixbuf_get_from_window(root_window, *window_geometry)
        surface = Gdk.cairo_surface_create_from_pixbuf(pixbuf, 1, None)
        surface.write_to_png(source_path)
        command = ["convert", source_path, "-blur", "0x5", target_path]
        subprocess = Gio.Subprocess.new(command, Gio.SubprocessFlags.NONE)
        subprocess.wait(None)
        return cairo.ImageSurface.create_from_png(target_path)

    def __init__(self, parent):
        self._parent = parent
        surface = self._get_base_surface()
        Painters.paint(surface)
        _, source_path = GLib.file_open_tmp(None)
        surface.write_to_png(source_path)
        command = ["i3lock", "-i", source_path]
        subprocess = Gio.Subprocess.new(command, Gio.SubprocessFlags.NONE)
        subprocess.wait(None)
