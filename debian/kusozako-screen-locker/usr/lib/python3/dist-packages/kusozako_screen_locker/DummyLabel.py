
# (c) copyright 2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaDummyLabel(Gtk.Label, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, "Ready ?", hexpand=True, vexpand=True)
        self._raise("delta > css", (self, "primary-surface-color-class"))
        self._raise("delta > add to container", self)
