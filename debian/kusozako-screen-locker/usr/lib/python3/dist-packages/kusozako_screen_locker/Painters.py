
# (c) copyright 2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import cairo
from gi.repository import Pango
from gi.repository import PangoCairo
from libkusozako3 import GtkFont


def _paint_shade(surface, cairo_context):
    cairo_context.set_source_rgba(0, 0, 0, 0.5)
    geometry = 0, 0, surface.get_width(), surface.get_height()
    cairo_context.rectangle(*geometry)
    cairo_context.fill()


def _paint_text(surface, cairo_context):
    cairo_context.set_source_rgba(1, 1, 1, 1)
    layout = PangoCairo.create_layout(cairo_context)
    layout.set_alignment(Pango.Alignment.CENTER)
    layout.set_markup("INPUT PASSWORD TO UNLOCK", -1)
    font_description = GtkFont.get_description(3)
    layout.set_font_description(font_description)
    _, rectangle_logical = layout.get_extents()
    surface_height = surface.get_height()
    layout.set_height(Pango.SCALE*surface_height)
    layout.set_width(Pango.SCALE*surface.get_width())
    layout_height = rectangle_logical.height/Pango.SCALE
    position_y = (surface_height-layout_height)/2
    cairo_context.move_to(0, position_y)
    PangoCairo.update_layout(cairo_context, layout)
    PangoCairo.show_layout(cairo_context, layout)


def paint(surface):
    cairo_context = cairo.Context(surface)
    _paint_shade(surface, cairo_context)
    _paint_text(surface, cairo_context)
