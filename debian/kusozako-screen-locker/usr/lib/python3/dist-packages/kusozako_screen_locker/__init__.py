
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gettext
import locale

VERSION = "2022.03.13"
APPLICATION_NAME = "kusozako-screen-locker"
APPLICATION_ID = "com.gitlab.kusozako-tech.kusozako-screen-locker"

locale.setlocale(locale.LC_ALL, None)
gettext.install(
    APPLICATION_NAME,
    "/usr/share/locale",
    names=('gettext', 'ngettext')
    )

APPLICATION_DATA = {
    "name": APPLICATION_NAME,
    "id": APPLICATION_ID,
    "icon-name": APPLICATION_ID,
    "version": VERSION,
    "short description": _("screen locker"),
    "long-description": _("""screen locker for kusozako project""")
    }
