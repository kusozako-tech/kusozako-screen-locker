#!/usr/bin/env python3

# (c) copyright 2022, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_screen_locker.MainLoop import DeltaMainLoop


if __name__ == "__main__":
    DeltaMainLoop()
